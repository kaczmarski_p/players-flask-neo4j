from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField
from wtforms.validators import DataRequired, Length, NumberRange


class PlayerForm(FlaskForm):
    player_name = StringField('Player long name', validators=[DataRequired(), Length(min=2, max=40)])
    submit = SubmitField('Search')


class ClubForm(FlaskForm):
    club_name = StringField('Club name', validators=[DataRequired(), Length(min=2, max=40)])
    submit = SubmitField('Search')


class PlayerYearForm(FlaskForm):
    player_name = StringField('Player long name', validators=[DataRequired(), Length(min=2, max=40)])
    year = IntegerField('In year', validators=[DataRequired(), NumberRange(min=2015, max=2020)])
    submit = SubmitField('Search')


class ClubYearForm(FlaskForm):
    club_name = StringField('Club name', validators=[DataRequired(), Length(min=2, max=40)])
    year = IntegerField('In year', validators=[DataRequired(), NumberRange(min=2015, max=2020)])
    submit = SubmitField('Search')


class TwoPlayerForm(FlaskForm):
    first_player = StringField('First player long name', validators=[DataRequired(), Length(min=2, max=40)])
    second_player = StringField('Second player long name', validators=[DataRequired(), Length(min=2, max=40)])
    submit = SubmitField('Search')


class RandomWalkForm(FlaskForm):
    player_name = StringField('Player long name', validators=[DataRequired(), Length(min=2, max=40)])
    steps = IntegerField('Number of steps', validators=[DataRequired(), NumberRange(min=1, max=999)])
    submit = SubmitField('Search')


class PageRankForm(FlaskForm):
    submit = SubmitField('Perform page rank')


class ClubYearsForm(FlaskForm):
    club_name = StringField('Club name', validators=[DataRequired(), Length(min=2, max=40)])
    start_year = IntegerField('Start year', validators=[DataRequired(), NumberRange(min=2015, max=2019)])
    end_year = IntegerField('End year', validators=[DataRequired(), NumberRange(min=2016, max=2020)])
    submit = SubmitField('Search')
