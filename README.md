## Data Science with knowledge graphs - neo4j
### Data mining on FIFA players dataset

This project is a web application using Flask connestion to neo4j database. In the neo4j there are graphs representing FIFA players dataset. The dataset is available here: [FIFA Players dataset](https://www.kaggle.com/stefanoleone992/fifa-20-complete-player-dataset). It consist of players statistics in years 2015-2020. The purpouse of the application is to analyze and demonstate the possibilities of Data Science plugin for neo4j graph database.

The application is placed in the two docker containers: one with Flask app and the other with the neo4j database. For a simple start, clone the repo and run the folowing command:
```
docker-compose up
```
This requires docker and docker compose installed. Containers run on localshost.

Neo4j folder contains volumes for the database: loaded data, import csv files and neo4j GDS plugin. The file queries.cypher holds all necessary queries to restore data from csv files (in case of deletion).

Other folders and files are connected with the flask application:
* *flask_page.py* - rendering views based on url and type of request
* *models.py* - backend with Py2neo plugin. Contains queries and fetches results from the neo4j database
* *forms.py* - WTForms used on pages
* *templates* - folder with html pages templates
* *static* - css configuration
