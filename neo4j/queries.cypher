:BEGIN
load csv with headers from 'file:///players/players_15.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2015}]->(m)
set n.overall_2015 = toInteger(row.overall)
set n.potential_2015 = toInteger(row.potential)
set n.pace_2015 = toInteger(row.pace)
set n.shooting_2015 = toInteger(row.shooting)
set n.passing_2015 = toInteger(row.passing)
set n.dribbling_2015 = toInteger(row.dribbling)
set n.defending_2015 = toInteger(row.defending)
set n.physic_2015 = toInteger(row.physic)
set n.gk_diving_2015 = toInteger(row.gk_diving)
set n.gk_handling_2015 = toInteger(row.gk_handling)
set n.gk_kicking_2015 = toInteger(row.gk_kicking)
set n.gk_reflexes_2015 = toInteger(row.gk_reflexes)
set n.gk_speed_2015 = toInteger(row.gk_speed)
set n.gk_positioning_2015 = toInteger(row.gk_positioning)
set n.value_eur_2015 = toInteger(row.value_eur)
set n.wage_eur_2015 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2015) as overall, avg(n.potential_2015) as potential, avg(n.pace_2015) as pace,
avg(n.shooting_2015) as shooting, avg(n.passing_2015) as passing, avg(n.dribbling_2015) as dribbling,
avg(n.defending_2015) as defending, avg(n.physic_2015) as physic, avg(n.gk_diving_2015) as gk_diving,
avg(n.gk_handling_2015) as gk_handling, avg(n.gk_kicking_2015) as gk_kicking, avg(n.gk_reflexes_2015) as gk_reflexes,
avg(n.gk_speed_2015) as gk_speed, avg(n.gk_positioning_2015) as gk_positioning,
avg(n.value_eur_2015) as value_eur, avg(n.wage_eur_2015) as wage_eur
set m.overall_2015 = overall
set m.potential_2015 = potential
set m.pace_2015 = pace
set m.shooting_2015 = shooting
set m.passing_2015 = passing
set m.dribbling_2015 = dribbling
set m.defending_2015 = defending
set m.physic_2015 = physic
set m.gk_diving_2015 = gk_diving
set m.gk_handling_2015 = gk_handling
set m.gk_kicking_2015 = gk_kicking
set m.gk_reflexes_2015 = gk_reflexes
set m.gk_speed_2015 = gk_speed
set m.gk_positioning_2015 = gk_positioning
set m.value_eur_2015 = value_eur
set m.wage_eur_2015 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_15.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2015}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_15.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2015}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_16.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2016}]->(m)
set n.overall_2016 = toInteger(row.overall)
set n.potential_2016 = toInteger(row.potential)
set n.pace_2016 = toInteger(row.pace)
set n.shooting_2016 = toInteger(row.shooting)
set n.passing_2016 = toInteger(row.passing)
set n.dribbling_2016 = toInteger(row.dribbling)
set n.defending_2016 = toInteger(row.defending)
set n.physic_2016 = toInteger(row.physic)
set n.gk_diving_2016 = toInteger(row.gk_diving)
set n.gk_handling_2016 = toInteger(row.gk_handling)
set n.gk_kicking_2016 = toInteger(row.gk_kicking)
set n.gk_reflexes_2016 = toInteger(row.gk_reflexes)
set n.gk_speed_2016 = toInteger(row.gk_speed)
set n.gk_positioning_2016 = toInteger(row.gk_positioning)
set n.value_eur_2016 = toInteger(row.value_eur)
set n.wage_eur_2016 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2016) as overall, avg(n.potential_2016) as potential, avg(n.pace_2016) as pace,
avg(n.shooting_2016) as shooting, avg(n.passing_2016) as passing, avg(n.dribbling_2016) as dribbling,
avg(n.defending_2016) as defending, avg(n.physic_2016) as physic, avg(n.gk_diving_2016) as gk_diving,
avg(n.gk_handling_2016) as gk_handling, avg(n.gk_kicking_2016) as gk_kicking, avg(n.gk_reflexes_2016) as gk_reflexes,
avg(n.gk_speed_2016) as gk_speed, avg(n.gk_positioning_2016) as gk_positioning,
avg(n.value_eur_2016) as value_eur, avg(n.wage_eur_2016) as wage_eur
set m.overall_2016 = overall
set m.potential_2016 = potential
set m.pace_2016 = pace
set m.shooting_2016 = shooting
set m.passing_2016 = passing
set m.dribbling_2016 = dribbling
set m.defending_2016 = defending
set m.physic_2016 = physic
set m.gk_diving_2016 = gk_diving
set m.gk_handling_2016 = gk_handling
set m.gk_kicking_2016 = gk_kicking
set m.gk_reflexes_2016 = gk_reflexes
set m.gk_speed_2016 = gk_speed
set m.gk_positioning_2016 = gk_positioning
set m.value_eur_2016 = value_eur
set m.wage_eur_2016 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_16.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2016}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_16.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2016}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_17.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2017}]->(m)
set n.overall_2017 = toInteger(row.overall)
set n.potential_2017 = toInteger(row.potential)
set n.pace_2017 = toInteger(row.pace)
set n.shooting_2017 = toInteger(row.shooting)
set n.passing_2017 = toInteger(row.passing)
set n.dribbling_2017 = toInteger(row.dribbling)
set n.defending_2017 = toInteger(row.defending)
set n.physic_2017 = toInteger(row.physic)
set n.gk_diving_2017 = toInteger(row.gk_diving)
set n.gk_handling_2017 = toInteger(row.gk_handling)
set n.gk_kicking_2017 = toInteger(row.gk_kicking)
set n.gk_reflexes_2017 = toInteger(row.gk_reflexes)
set n.gk_speed_2017 = toInteger(row.gk_speed)
set n.gk_positioning_2017 = toInteger(row.gk_positioning)
set n.value_eur_2017 = toInteger(row.value_eur)
set n.wage_eur_2017 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2017) as overall, avg(n.potential_2017) as potential, avg(n.pace_2017) as pace,
avg(n.shooting_2017) as shooting, avg(n.passing_2017) as passing, avg(n.dribbling_2017) as dribbling,
avg(n.defending_2017) as defending, avg(n.physic_2017) as physic, avg(n.gk_diving_2017) as gk_diving,
avg(n.gk_handling_2017) as gk_handling, avg(n.gk_kicking_2017) as gk_kicking, avg(n.gk_reflexes_2017) as gk_reflexes,
avg(n.gk_speed_2017) as gk_speed, avg(n.gk_positioning_2017) as gk_positioning,
avg(n.value_eur_2017) as value_eur, avg(n.wage_eur_2017) as wage_eur
set m.overall_2017 = overall
set m.potential_2017 = potential
set m.pace_2017 = pace
set m.shooting_2017 = shooting
set m.passing_2017 = passing
set m.dribbling_2017 = dribbling
set m.defending_2017 = defending
set m.physic_2017 = physic
set m.gk_diving_2017 = gk_diving
set m.gk_handling_2017 = gk_handling
set m.gk_kicking_2017 = gk_kicking
set m.gk_reflexes_2017 = gk_reflexes
set m.gk_speed_2017 = gk_speed
set m.gk_positioning_2017 = gk_positioning
set m.value_eur_2017 = value_eur
set m.wage_eur_2017 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_17.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2017}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_17.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2017}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_18.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2018}]->(m)
set n.overall_2018 = toInteger(row.overall)
set n.potential_2018 = toInteger(row.potential)
set n.pace_2018 = toInteger(row.pace)
set n.shooting_2018 = toInteger(row.shooting)
set n.passing_2018 = toInteger(row.passing)
set n.dribbling_2018 = toInteger(row.dribbling)
set n.defending_2018 = toInteger(row.defending)
set n.physic_2018 = toInteger(row.physic)
set n.gk_diving_2018 = toInteger(row.gk_diving)
set n.gk_handling_2018 = toInteger(row.gk_handling)
set n.gk_kicking_2018 = toInteger(row.gk_kicking)
set n.gk_reflexes_2018 = toInteger(row.gk_reflexes)
set n.gk_speed_2018 = toInteger(row.gk_speed)
set n.gk_positioning_2018 = toInteger(row.gk_positioning)
set n.value_eur_2018 = toInteger(row.value_eur)
set n.wage_eur_2018 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2018) as overall, avg(n.potential_2018) as potential, avg(n.pace_2018) as pace,
avg(n.shooting_2018) as shooting, avg(n.passing_2018) as passing, avg(n.dribbling_2018) as dribbling,
avg(n.defending_2018) as defending, avg(n.physic_2018) as physic, avg(n.gk_diving_2018) as gk_diving,
avg(n.gk_handling_2018) as gk_handling, avg(n.gk_kicking_2018) as gk_kicking, avg(n.gk_reflexes_2018) as gk_reflexes,
avg(n.gk_speed_2018) as gk_speed, avg(n.gk_positioning_2018) as gk_positioning,
avg(n.value_eur_2018) as value_eur, avg(n.wage_eur_2018) as wage_eur
set m.overall_2018 = overall
set m.potential_2018 = potential
set m.pace_2018 = pace
set m.shooting_2018 = shooting
set m.passing_2018 = passing
set m.dribbling_2018 = dribbling
set m.defending_2018 = defending
set m.physic_2018 = physic
set m.gk_diving_2018 = gk_diving
set m.gk_handling_2018 = gk_handling
set m.gk_kicking_2018 = gk_kicking
set m.gk_reflexes_2018 = gk_reflexes
set m.gk_speed_2018 = gk_speed
set m.gk_positioning_2018 = gk_positioning
set m.value_eur_2018 = value_eur
set m.wage_eur_2018 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_18.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2018}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_18.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2018}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_19.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2019}]->(m)
set n.overall_2019 = toInteger(row.overall)
set n.potential_2019 = toInteger(row.potential)
set n.pace_2019 = toInteger(row.pace)
set n.shooting_2019 = toInteger(row.shooting)
set n.passing_2019 = toInteger(row.passing)
set n.dribbling_2019 = toInteger(row.dribbling)
set n.defending_2019 = toInteger(row.defending)
set n.physic_2019 = toInteger(row.physic)
set n.gk_diving_2019 = toInteger(row.gk_diving)
set n.gk_handling_2019 = toInteger(row.gk_handling)
set n.gk_kicking_2019 = toInteger(row.gk_kicking)
set n.gk_reflexes_2019 = toInteger(row.gk_reflexes)
set n.gk_speed_2019 = toInteger(row.gk_speed)
set n.gk_positioning_2019 = toInteger(row.gk_positioning)
set n.value_eur_2019 = toInteger(row.value_eur)
set n.wage_eur_2019 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2019) as overall, avg(n.potential_2019) as potential, avg(n.pace_2019) as pace,
avg(n.shooting_2019) as shooting, avg(n.passing_2019) as passing, avg(n.dribbling_2019) as dribbling,
avg(n.defending_2019) as defending, avg(n.physic_2019) as physic, avg(n.gk_diving_2019) as gk_diving,
avg(n.gk_handling_2019) as gk_handling, avg(n.gk_kicking_2019) as gk_kicking, avg(n.gk_reflexes_2019) as gk_reflexes,
avg(n.gk_speed_2019) as gk_speed, avg(n.gk_positioning_2019) as gk_positioning,
avg(n.value_eur_2019) as value_eur, avg(n.wage_eur_2019) as wage_eur
set m.overall_2019 = overall
set m.potential_2019 = potential
set m.pace_2019 = pace
set m.shooting_2019 = shooting
set m.passing_2019 = passing
set m.dribbling_2019 = dribbling
set m.defending_2019 = defending
set m.physic_2019 = physic
set m.gk_diving_2019 = gk_diving
set m.gk_handling_2019 = gk_handling
set m.gk_kicking_2019 = gk_kicking
set m.gk_reflexes_2019 = gk_reflexes
set m.gk_speed_2019 = gk_speed
set m.gk_positioning_2019 = gk_positioning
set m.value_eur_2019 = value_eur
set m.wage_eur_2019 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_19.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2019}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_19.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2019}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_20.csv' as row
merge (n: Player {name: row.long_name})
merge (m: Club {name: row.club})
merge (n)-[r: PLAYS_IN {year: 2020}]->(m)
set n.overall_2020 = toInteger(row.overall)
set n.potential_2020 = toInteger(row.potential)
set n.pace_2020 = toInteger(row.pace)
set n.shooting_2020 = toInteger(row.shooting)
set n.passing_2020 = toInteger(row.passing)
set n.dribbling_2020 = toInteger(row.dribbling)
set n.defending_2020 = toInteger(row.defending)
set n.physic_2020 = toInteger(row.physic)
set n.gk_diving_2020 = toInteger(row.gk_diving)
set n.gk_handling_2020 = toInteger(row.gk_handling)
set n.gk_kicking_2020 = toInteger(row.gk_kicking)
set n.gk_reflexes_2020 = toInteger(row.gk_reflexes)
set n.gk_speed_2020 = toInteger(row.gk_speed)
set n.gk_positioning_2020 = toInteger(row.gk_positioning)
set n.value_eur_2020 = toInteger(row.value_eur)
set n.wage_eur_2020 = toInteger(row.wage_eur);
:COMMIT

:BEGIN
match (n: Player)-[r]->(m: Club)
with m, avg(n.overall_2020) as overall, avg(n.potential_2020) as potential, avg(n.pace_2020) as pace,
avg(n.shooting_2020) as shooting, avg(n.passing_2020) as passing, avg(n.dribbling_2020) as dribbling,
avg(n.defending_2020) as defending, avg(n.physic_2020) as physic, avg(n.gk_diving_2020) as gk_diving,
avg(n.gk_handling_2020) as gk_handling, avg(n.gk_kicking_2020) as gk_kicking, avg(n.gk_reflexes_2020) as gk_reflexes,
avg(n.gk_speed_2020) as gk_speed, avg(n.gk_positioning_2020) as gk_positioning,
avg(n.value_eur_2020) as value_eur, avg(n.wage_eur_2020) as wage_eur
set m.overall_2020 = overall
set m.potential_2020 = potential
set m.pace_2020 = pace
set m.shooting_2020 = shooting
set m.passing_2020 = passing
set m.dribbling_2020 = dribbling
set m.defending_2020 = defending
set m.physic_2020 = physic
set m.gk_diving_2020 = gk_diving
set m.gk_handling_2020 = gk_handling
set m.gk_kicking_2020 = gk_kicking
set m.gk_reflexes_2020 = gk_reflexes
set m.gk_speed_2020 = gk_speed
set m.gk_positioning_2020 = gk_positioning
set m.value_eur_2020 = value_eur
set m.wage_eur_2020 = wage_eur;
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_20.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_tags, ',') AS skill
merge (o: Tag {name: toLower(replace(replace(skill, '#', ''),' ', ''))})
merge (n)-[r: HAS_SKILL {year: 2020}]->(o);
:COMMIT

:BEGIN
load csv with headers from 'file:///players/players_20.csv' as row
match (n: Player {name: row.long_name})
unwind split(row.player_positions, ',') AS position
merge (o: Position {name: toLower(replace(position,' ', ''))})
merge (n)-[r: ON_POSITION {year: 2020}]->(o);
:COMMIT

:BEGIN
match (n: Player)
set n: Page_Rank;
:COMMIT

:BEGIN
match (n: Tag)
set n: Page_Rank;
:COMMIT

:BEGIN
match (n: Player)-[r]-(o: Tag)
set n: Randomwalk
set o: Randomwalk;
:COMMIT

:BEGIN
match (n: Player)-[r: HAS_SKILL]->(o: Tag)
merge (n)-[p: RADNOMWALK]->(o)
merge (o)-[k: RADNOMWALK]->(n);
:COMMIT

:BEGIN
match (n: Player)-[r]-(o: Club)
set n: ShortestPath
set o: ShortestPath;
:COMMIT
