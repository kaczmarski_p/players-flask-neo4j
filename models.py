from py2neo import Graph, Node, Relationship
import os

# Set up the connection to neo4j
graph = Graph('http://neo4j-players:7474' + '/db/data/')


def filter_null_and_create_dict_list(dictionary):
    filtered_list = []
    for key, value in dictionary.items():
        if value is not None:
            filtered_list.append({key: value})
    return filtered_list


def filter_node_name(node_name, data, round_up):
    new_dict = {}
    for key, value in data.items():
        if round_up:
            new_dict[key.replace(f'{node_name}.', '')] = round(value, 2)
        else:
            new_dict[key.replace(f'{node_name}.', '')] = value
    return new_dict


# Matches player by the full name and returns node properties.
# The "gk" represents goalkeeping skills.
def get_player_stats(player_name, year):
    query = f'''
            MATCH (n: Player {{name: "{player_name}"}})-[r: PLAYS_IN {{year: {year}}}]->(m: Club)
            RETURN n.overall_{year}, n.potential_{year}, n.pace_{year}, n.shooting_{year}, n.passing_{year},
            n.dribbling_{year}, n.defending_{year}, n.physic_{year}, n.gk_diving_{year}, n.gk_handling_{year},
            n.gk_kicking_{year}, n.gk_reflexes_{year}, n.gk_speed_{year}, n.gk_positioning_{year}
            '''
    data = graph.run(query).data()
    new_dict = filter_node_name('n', data[0], False)

    return filter_null_and_create_dict_list(new_dict)


# Matches club by the full name and returns node properties.
# Club properties are pre-calculated in the database, so there is no need to compute them in this query.
def get_club_stats(club_name, year):
    query = f'''
            MATCH (m: Player)-[r: PLAYS_IN {{year: {year}}}]->(n: Club  {{name: "{club_name}"}})
            RETURN n.overall_{year}, n.potential_{year}, n.pace_{year}, n.shooting_{year}, n.passing_{year},
            n.dribbling_{year}, n.defending_{year}, n.physic_{year}, n.gk_diving_{year}, n.gk_handling_{year},
            n.gk_kicking_{year}, n.gk_reflexes_{year}, n.gk_speed_{year}, n.gk_positioning_{year}
            '''
    data = graph.run(query).data()
    new_dict = filter_node_name('n', data[0], True)

    return filter_null_and_create_dict_list(new_dict)


# Matches the source player by full name.
# Next, it gets all other players and calculates the list of absolute differences of properties.
# The list is reduced and the base dissimilarity is set as sum of the differences.
# Players with the lowest dissimilarity are collected.
# For this 10 players, their positions are gathered.
# With the positions of source player and the other players positions,
# the Jaccard node similarity can be computed between them.
# Query returns the initial result.
# The for loop calculates the total dissimilarity score, which is equal to: base_diss + (1-node_similarity)*5
def get_similar_player(player_name, year):
    query = f'''
            MATCH (source: Player {{name: "{player_name}"}})
            WITH source
            MATCH (n: Player)-[: PLAYS_IN {{year: {year}}}]->(c: Club)
            WHERE n.name <> source.name and n.shooting_{year} is not null
            SET n.dissimilarity = [abs(n.overall_{year} - source.overall_{year}), 
            abs(n.potential_{year} - source.potential_{year}), 
            abs(n.pace_{year} - source.pace_{year}), 
            abs(n.shooting_{year} - source.shooting_{year}), 
            abs(n.passing_{year} - source.passing_{year}), 
            abs(n.dribbling_{year} - source.dribbling_{year}), 
            abs(n.defending_{year} - source.defending_{year})]
            SET n.dissimilarity = reduce(acc= 0, v in n.dissimilarity | acc + v)
            WITH source, n order by n.dissimilarity limit 10
            MATCH (source)-[r1: ON_POSITION {{year: {year}}}]->(p1: Position)
            WITH source, n, collect(id(p1)) AS pos1
            MATCH (n)-[r2: ON_POSITION {{year: {year}}}]->(p2: Position)
            WITH n, pos1, collect(id(p2)) AS pos2
            RETURN n.name, n.overall_{year} as overall,
            n.dissimilarity, gds.alpha.similarity.jaccard(pos1, pos2) AS node_similarity
            '''
    data = graph.run(query).data()

    new_list = []
    for player in data:
        new_dict = filter_node_name('n', player, False)
        new_list.append(new_dict)

    for player in new_list:
        player['diss'] = round(player['dissimilarity'] + (1-player['node_similarity'])*5, 2)

    sorted_list = sorted(new_list, key=lambda i: i['diss'], reverse=False)
    return sorted_list


# A first query defines graph based on node label and relationship type.
# The tags and relationships were pre-set up in the neo4j
# A second query matches the player, calls the GDS function with certain number of steps and retrieves the result nodes.
# Third query deletes the created graph as it is needed no more.
def get_random_walk(player_name, steps):
    query = f'''
            CALL gds.graph.create('randomwalk_tags', 'Randomwalk', 'RADNOMWALK');
            '''
    graph.run(query)

    query2 = f'''
             MATCH (n: Player {{name: "{player_name}"}})
             CALL gds.alpha.randomWalk.stream('randomwalk_tags', {{steps: {steps}, start: id(n)}})
             YIELD nodeIds
             UNWIND nodeIds AS nodeId
             WITH gds.util.asNode(nodeId) AS nodes
             RETURN head(labels(nodes)) AS label, nodes.name AS node_name
             '''
    data = graph.run(query2).data()

    query3 = f'''
             CALL gds.graph.drop('randomwalk_tags');
             '''
    graph.run(query3)

    return data


# A first query defines graph based on node label and relationship type.
# The tags and were pre-set up in the neo4j. Relationships between players and clubs are "PLAYS_IN".
# As we use undirected orientation, there is no need to set up the new relationships.
# A second query matches both players, calls the GDS function and retrieves the result nodes.
# Third query deletes the created graph as it is needed no more.
def get_shortest_path(first_player_name, second_player_name):
    query = f'''
            CALL gds.graph.create('sp_players', 'ShortestPath', {{PLAYS_WITH: {{type:'PLAYS_IN', orientation:'Undirected'}}}})
            '''
    graph.run(query)

    query2 = f'''
             MATCH (source: Player {{name: "{first_player_name}"}})
             MATCH (destination: Player {{name: "{second_player_name}"}})
             CALL gds.alpha.shortestPath.stream('sp_players', {{startNode:source, endNode: destination, relationshipWeightProperty: null}})
             YIELD nodeId, cost
             WITH gds.util.asNode(nodeId) AS nodes, cost
             RETURN head(labels(nodes)) AS label, nodes.name AS node_name, cost
             '''
    data = graph.run(query2).data()

    query3 = f'''
             CALL gds.graph.drop('sp_players')
             '''
    graph.run(query3)

    return data


# A first query defines graph based on node label and relationship type.
# The tags and were pre-set up in the neo4j. Relationships between players and ability tags are "HAS_SKILL".
# The relationships are directed, so the page rank can count the followers.
# A second query calls the GDS function and retrieves the result nodes.
# There are 17 skills so we retrieve first 17 scores (the rest are player nodes).
# Third query deletes the created graph as it is needed no more.
def get_page_rank():
    query = f'''
            CALL gds.graph.create('pagerank_tags', 'Page_Rank', 'HAS_SKILL')
            '''
    graph.run(query)

    query2 = f'''
             CALL gds.pageRank.stream('pagerank_tags',{{maxIterations: 20,  dampingFactor: 0.85}}) YIELD nodeId, score
             RETURN gds.util.asNode(nodeId).name AS name, score ORDER BY score DESC LIMIT 17
             '''
    data = graph.run(query2).data()

    query3 = f'''
             CALL gds.graph.drop('pagerank_tags')
             '''
    graph.run(query3)

    return data


# Query matches certain sets of players connected to the club in the specified years.
# core_players are present in the club at both start year and end year,
# old_players only at a start year and new_players only at an end year
# Next, it calculates sum of player values and wages for each set, to build statistics.
# The decision if the club invests heavily in the core players is based on the "a" coefficient of the linear function.
def get_money_stats(club_name, start_year, end_year):
    query = f'''
            MATCH (a: Player)-[:PLAYS_IN {{year: {start_year}}}]->(c: Club {{name: "{club_name}"}})
            MATCH (b: Player)-[:PLAYS_IN {{year: {end_year}}}]->(c: Club {{name: "{club_name}"}})
            where (a.name = b.name)
            with a as core_players
            MATCH (a: Player)-[PLAYS_IN {{year: {start_year}}}]->(c: Club {{name: "{club_name}"}})
            WHERE not (a)-[:PLAYS_IN {{year: {end_year}}}]->(c)
            AND a.value_eur_{end_year} is not null
            AND a. wage_eur_{end_year} is not null
            WITH core_players, a as old_players
            MATCH (a: Player)-[PLAYS_IN {{year: {end_year}}}]->(c: Club {{name: "{club_name}"}})
            WHERE not (a)-[:PLAYS_IN {{year: {start_year}}}]->(c)
            AND a.value_eur_{start_year} is not null
            AND a. wage_eur_{start_year} is not null
            WITH core_players, old_players, a as new_players
            MATCH (players_{start_year}: Player)-[:PLAYS_IN {{year: {start_year}}}]->(c: Club {{name: "{club_name}"}})
            MATCH (players_{end_year}: Player)-[:PLAYS_IN {{year: {end_year}}}]->(c: Club {{name: "{club_name}"}})
            RETURN sum(players_{start_year}.value_eur_{start_year}) as all_players_value_eur_{start_year},
            sum(players_{end_year}.value_eur_{end_year}) as all_players_value_eur_{end_year},
            sum(players_{start_year}.wage_eur_{start_year}) as all_players_wage_eur_{start_year},
            sum(players_{end_year}.wage_eur_{end_year}) as all_players_wage_eur_{end_year},
            sum(core_players.value_eur_{start_year}) as core_players_value_eur_{start_year},
            sum(core_players.value_eur_{end_year}) as core_players_value_eur_{end_year},
            sum(core_players.wage_eur_{start_year}) as core_players_wage_eur_{start_year},
            sum(core_players.wage_eur_{end_year}) as core_players_wage_eur_{end_year},
            sum(old_players.value_eur_{start_year}) as old_players_value_eur_{start_year},
            sum(old_players.value_eur_{end_year}) as old_players_value_eur_{end_year},
            sum(old_players.wage_eur_{start_year}) as old_players_wage_eur_{start_year},
            sum(old_players.wage_eur_{end_year}) as old_players_wage_eur_{end_year},
            sum(new_players.value_eur_{start_year}) as new_players_value_eur_{start_year},
            sum(new_players.value_eur_{end_year}) as new_players_value_eur_{end_year},
            sum(new_players.wage_eur_{start_year}) as new_players_wage_eur_{start_year},
            sum(new_players.wage_eur_{end_year}) as new_players_wage_eur_{end_year}
            '''
    data = graph.run(query).data()

    core_start = data[0][f'core_players_value_eur_{start_year}']
    core_end = data[0][f'core_players_value_eur_{end_year}']
    a = (core_end-core_start)/(1e9*(end_year-start_year))
    if a > 1500:
        talent_forge = True
    else:
        talent_forge = False

    return data[0], talent_forge


# Queries match certain sets of players connected to the club in the specified years.
# query - core_players are present in the club at both start year and end year.
# query2 - old_players - only present at a start year.
# query3 - new_players - only present at an end year.
# They also fetch the destination/source clubs of the outgoing/incoming players.
# The decision if the club invests has heavy rotation cannot be a linear function.
# The result is determined by the year difference and number of core_players left.
def get_rotation(club_name, start_year, end_year):
    query = f'''
            MATCH (a: Player)-[:PLAYS_IN {{year: {start_year}}}]->(c: Club {{name: "{club_name}"}})
            MATCH (b: Player)-[:PLAYS_IN {{year: {end_year}}}]->(c: Club {{name: "{club_name}"}})
            WHERE (a.name = b.name)
            RETURN a.name as core_player, c.name as club
            '''
    core_players = graph.run(query).data()

    query2 = f'''
            MATCH (a: Player)-[PLAYS_IN {{year: {start_year}}}]->(c: Club {{name: "{club_name}"}})
            WHERE not (a)-[:PLAYS_IN {{year: {end_year}}}]->(c)
            WITH a as old_player
            MATCH (old_player)-[p: PLAYS_IN {{year: {end_year}}}]->(a: Club)
            RETURN old_player.name as old_player, a.name as club
            '''
    old_players = graph.run(query2).data()

    query3 = f'''
            MATCH (a: Player)-[PLAYS_IN {{year: {end_year}}}]->(c: Club {{name: "{club_name}"}})
            WHERE not (a)-[:PLAYS_IN {{year: {start_year}}}]->(c)
            WITH a as new_player
            MATCH (new_player)-[p: PLAYS_IN {{year: {start_year}}}]->(a: Club)
            RETURN new_player.name as new_player, a.name as club
            '''
    new_players = graph.run(query3).data()

    year_diff = end_year-start_year
    rotate = False
    if year_diff == 1:
        if len(core_players) < 19:
            rotate = True
    elif year_diff == 2:
        if len(core_players) < 15:
            rotate = True
    elif year_diff == 3:
        if len(core_players) < 12:
            rotate = True
    elif year_diff == 4:
        if len(core_players) < 10:
            rotate = True
    elif year_diff == 5:
        if len(core_players) < 9:
            rotate = True

    all_players = core_players + old_players + new_players
    result_list = []
    for dict_item in all_players:
        row_list = []
        for key, value in dict_item.items():
            if key != 'club':
                row_list.append(key)
            row_list.append(value)
        result_list.append(row_list)

    return result_list, rotate
