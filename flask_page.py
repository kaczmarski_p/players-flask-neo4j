from flask import Flask, render_template, url_for, flash, redirect
from forms import *
from models import *
app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'


@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html', title='Home')


@app.route('/player_stats', methods=['GET', 'POST'])
def player_stats():
    form = PlayerYearForm()
    if form.validate_on_submit():
        flash(f'Searching stats for player {form.player_name.data} in year {form.year.data}...', 'success')
        result = get_player_stats(form.player_name.data, form.year.data)
        return render_template('player_stats.html', title='Player stats', form=form, result=result)
    return render_template('player_stats.html', title='Player stats', form=form)


@app.route('/club_stats', methods=['GET', 'POST'])
def club_stats():
    form = ClubYearForm()
    if form.validate_on_submit():
        flash(f'Searching stats for club {form.club_name.data} in year {form.year.data}...', 'success')
        result = get_club_stats(form.club_name.data, form.year.data)
        return render_template('club_stats.html', title='Club stats', form=form, result=result)
    return render_template('club_stats.html', title='Club stats', form=form)


@app.route('/similar', methods=['GET', 'POST'])
def similar():
    form = PlayerYearForm()
    if form.validate_on_submit():
        flash(f'Searching most similar player to {form.player_name.data} in year {form.year.data}...', 'success')
        result = get_similar_player(form.player_name.data, form.year.data)
        return render_template('similar.html', title='Similar player', form=form, result=result)
    return render_template('similar.html', title='Similar player', form=form)


@app.route('/random_walk', methods=['GET', 'POST'])
def random_walk():
    form = RandomWalkForm()
    if form.validate_on_submit():
        flash(f'Performing random walk starting from {form.player_name.data} having {form.steps.data} steps...', 'success')
        result = get_random_walk(form.player_name.data, form.steps.data)
        return render_template('random_walk.html', title='Random walk', form=form, result=result)
    return render_template('random_walk.html', title='Random walk', form=form)


@app.route('/distance', methods=['GET', 'POST'])
def distance():
    form = TwoPlayerForm()
    if form.validate_on_submit():
        flash(f'Calculating shortest path between {form.first_player.data} and {form.second_player.data} ...', 'success')
        result = get_shortest_path(form.first_player.data, form.second_player.data)
        return render_template('distance.html', title='Distance', form=form, result=result)
    return render_template('distance.html', title='Distance', form=form)


@app.route('/page_rank', methods=['GET', 'POST'])
def page_rank():
    form = PageRankForm()
    if form.validate_on_submit():
        flash(f'Preforming GDS page rank...', 'success')
        result = get_page_rank()
        return render_template('page_rank.html', title='Page rank', form=form, result=result)
    return render_template('page_rank.html', title='Page rank', form=form)


@app.route('/money_stats', methods=['GET', 'POST'])
def money_stats():
    form = ClubYearsForm()
    if form.validate_on_submit():
        flash(f'Calculating money statistics for club {form.club_name.data} between years {form.start_year.data} and {form.end_year.data}...',
              'success')
        result, forge = get_money_stats(form.club_name.data, form.start_year.data, form.end_year.data)
        return render_template('money_stats.html', title='Money stats', form=form, result=result, forge=forge)
    return render_template('money_stats.html', title='Money stats', form=form)


@app.route('/rotation', methods=['GET', 'POST'])
def rotation():
    form = ClubYearsForm()
    if form.validate_on_submit():
        flash(f'Player rotation in club {form.club_name.data} between years {form.start_year.data} and {form.end_year.data}...',
              'success')
        result, rotate = get_rotation(form.club_name.data, form.start_year.data, form.end_year.data)
        return render_template('rotation.html', title='Rotation', form=form, result=result, rotate=rotate)
    return render_template('rotation.html', title='Rotation', form=form)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
